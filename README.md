### README.md

# Attendance System

## Overview
The Attendance System is a Java-based application designed to manage student attendance for various subjects. This system allows users to take, edit, and report attendance efficiently.

## Features
- Take attendance for students.
- Edit and update attendance records.
- Generate attendance reports.
- Command pattern implementation for handling attendance operations.

## Getting Started

### Prerequisites
- Java Development Kit (JDK) 8 or higher
- MySQL database

### Installation

1. **Clone the Repository:**
   ```sh
   git clone https://gitlab.com/yourusername/AttendanceSystem.git
   cd AttendanceSystem
   ```

2. **Configure the Database:**
   - Set up a MySQL database using XAMPP.
   - Update the database connection settings in `DatabaseConnection.java`:
     ```java
     private static final String URL = "jdbc:mysql://localhost:3306/your_database_name";
     private static final String USER = "your_database_user";
     private static final String PASSWORD = "your_database_password";
     ```

3. **Compile and Run:**
   - Open the project in your preferred IDE (e.g., IntelliJ IDEA, Eclipse, VSCode).
   - Compile the project and run `App.java` to start the application.

## Usage

1. **Login:**
   - Use the login form to access the application.

2. **Take Attendance:**
   - Navigate to the "Take Attendance" tab.
   - Select the subject and mark students as present or absent.
   - Click the "Submit" button to save the attendance.

3. **Edit Attendance:**
   - Navigate to the "Edit Attendance" tab.
   - Select the date and load the attendance records.
   - Modify the attendance as needed and save the changes.

4. **Generate Reports:**
   - Navigate to the "Report" tab to generate and view attendance reports.

## Project Structure
- `src`: Contains the Java source files.
- `lib`: Contains necessary libraries.


## Roles and Contributions
- **Parjeet Mongar**: Project Leader, responsible for overall project management, coordination, and ensuring timely completion of milestones.
- **Pema Chozom**: Lead Developer, responsible for implementing the core functionalities and integrating the patterns.
- **Jigme Tenzin**: Database Administrator, responsible for setting up the MySQL database, writing SQL queries, and managing database connectivity.
- **Tenzin Namgyal Yeshi**: UI/UX Designer, responsible for designing the user interface, ensuring a smooth user experience, and implementing the frontend components.

## Contributing
If you would like to contribute to this project, please fork the repository and create a pull request with your changes. Ensure to follow the coding standards and write appropriate tests for your code.


## Contact
For any questions or inquiries, please contact(mailto:mongarparjeet@gmail.com).